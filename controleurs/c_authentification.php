<?php

/**
 * Gestion de la connexion
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
if (!$uc) {
    $uc = 'authentification';
}

switch ($action) {
    case 'demandeInscription':
        include 'vues/v_inscription.php';
        break;
    case 'valideInscription':
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $repeat_password = filter_input(INPUT_POST, 'repeat-password', FILTER_SANITIZE_STRING);
        // A tester
        $password = hash('sha256', $password);
        $repeat_password = hash('sha256', $repeat_password);
        if ($password == $repeat_password) {
            $user = $pdo->saveUser($email, $password);
            if (!is_array($user)) {
                ajouterErreur('Une erreure s\'est produite.');
                include 'vues/v_erreurs.php';
                include 'vues/v_inscription.php';
            } else {
                $id = $user['id'];
                $email = $user['email'];
                connecter($id, $email);
                header('Location: index.php');
            }
        } else {
            ajouterErreur('Les mots de passes ne correspondent pas.');
            include 'vues/v_erreurs.php';
            include 'vues/v_inscription.php';
        }
        break;
    case 'demandeConnexion':
        include 'vues/v_connexion.php';
        break;
    case 'valideConnexion':
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $password = hash('sha256', $password);
        $user = $pdo->getInfosUser($email, $password);
        if (!is_array($user)) {
            ajouterErreur('Email ou mot de passe incorrect');
            include 'vues/v_erreurs.php';
            include 'vues/v_connexion.php';
        } else {
            $id = $user['id'];
            $email = $user['email'];
            connecter($id, $email);
            header('Location: index.php'); //
        }
        break;

    case 'demandeDeconnexion':
        include 'vues/v_deconnexion.php';
        break;
    case 'valideDeconnexion':
        if (estConnecte()) {
            include 'vues/v_deconnexion.php';
        } else {
            ajouterErreur("Vous n'êtes pas connecté");
            include 'vues/v_erreurs.php';
            include 'vues/v_connexion.php';
        }
        break;
    default:
        include 'vues/v_connexion.php';
        break;
}
