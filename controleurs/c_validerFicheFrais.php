<?php

/**
 * Gestion des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

switch ($action) {
    case 'afficherFrais':
        // Affichage des frais forfaits et Hors Forfaits
        $idVisiteur = 'a131';
        $mois = '201809';

        // Edition frais HORS FORFAITS
        $dateFrais = filter_input(INPUT_POST, 'dateFrais', FILTER_SANITIZE_STRING);
        $libelle = filter_input(INPUT_POST, 'libelle', FILTER_SANITIZE_STRING);
        $montant = filter_input(INPUT_POST, 'montant', FILTER_VALIDATE_FLOAT);
        $idFraisHorsForfait = filter_input(INPUT_POST, 'montant', FILTER_VALIDATE_INT);
        
        if (isset($dateFrais) && isset($libelle) && isset($montant)) {
            if (!empty($dateFrais) && !empty($libelle) && !empty($montant)) {
                $pdo->majFraisHorsForfait($idFraisHorsForfait, $dateFrais, $libelle, $montant);
            } else {
                ajouterErreur('Vous ne pouvez pas laisser un champs vide.');
                include 'vues/v_erreurs.php';
            }
        }

        // Les frais hors forfaits
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
        include 'vues/v_afficherFrais.php';
        break;
}
//$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
//$lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
//require 'vues/v_listeFraisForfait.php';
//require 'vues/v_listeFraisHorsForfait.php';
