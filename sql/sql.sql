-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.26 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour eco_service
DROP DATABASE IF EXISTS `eco_service`;
CREATE DATABASE IF NOT EXISTS `eco_service` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `eco_service`;

-- Listage de la structure de la table eco_service. client
DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `mdp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_client_role` (`role_id`),
  CONSTRAINT `FK_client_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.client : ~3 rows (environ)
DELETE FROM `client`;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`id`, `role_id`, `nom`, `prenom`, `mail`, `mdp`) VALUES
	(1, 1, 'admin', 'adminp', 'admin@gmail.com', '123'),
	(2, 2, 'particulier', 'particulierp', 'particulier@gmail.com', '456'),
	(3, 3, 'professionnel', 'professionnelp', 'professionnel@gmail.com', '789');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. commande
DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `codepostal` varchar(50) DEFAULT NULL,
  `etatid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_commande_etatcommande` (`etatid`),
  CONSTRAINT `FK_commande_etatcommande` FOREIGN KEY (`etatid`) REFERENCES `etatcommande` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.commande : ~5 rows (environ)
DELETE FROM `commande`;
/*!40000 ALTER TABLE `commande` DISABLE KEYS */;
INSERT INTO `commande` (`id`, `adresse`, `ville`, `codepostal`, `etatid`) VALUES
	(1, '11 rue de la république', 'marseille', '13009', 1),
	(2, '20 rue de la république', 'marseille', '13008', 3),
	(3, '30 rue de la république', 'marseille', '13007', 2),
	(4, '40 rue de la république', 'marseille', '13006', 4),
	(5, '50 rue de la république', 'marseille', '13005', 5);
/*!40000 ALTER TABLE `commande` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. commander
DROP TABLE IF EXISTS `commander`;
CREATE TABLE IF NOT EXISTS `commander` (
  `idcommander` int(11) NOT NULL AUTO_INCREMENT,
  `quantite` int(11) DEFAULT '0',
  `factureid` int(11) DEFAULT '0',
  `serviceid` int(11) DEFAULT NULL,
  `clientid` int(11) DEFAULT '0',
  `produitid` int(11) DEFAULT NULL,
  `commandeid` int(11) DEFAULT '0',
  `date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idcommander`),
  KEY `FK_commander_produit` (`produitid`),
  KEY `FK_commander_commande` (`commandeid`),
  KEY `FK_commander_client` (`clientid`),
  KEY `FK_commander_facture` (`factureid`),
  KEY `FK_commander_service` (`serviceid`),
  CONSTRAINT `FK_commander_client` FOREIGN KEY (`clientid`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_commander_commande` FOREIGN KEY (`commandeid`) REFERENCES `commande` (`id`),
  CONSTRAINT `FK_commander_facture` FOREIGN KEY (`factureid`) REFERENCES `facture` (`id`),
  CONSTRAINT `FK_commander_produit` FOREIGN KEY (`produitid`) REFERENCES `produit` (`id`),
  CONSTRAINT `FK_commander_service` FOREIGN KEY (`serviceid`) REFERENCES `service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.commander : ~6 rows (environ)
DELETE FROM `commander`;
/*!40000 ALTER TABLE `commander` DISABLE KEYS */;
INSERT INTO `commander` (`idcommander`, `quantite`, `factureid`, `serviceid`, `clientid`, `produitid`, `commandeid`, `date`) VALUES
	(5, 4, 2, 3, 3, NULL, 1, '3 Janvier 2019'),
	(7, 8, 1, NULL, 2, 2, 2, '4 mars 2019'),
	(11, 1, 1, NULL, 2, 6, 2, '5 avril 2019'),
	(13, 2, 3, 1, 3, NULL, 3, '6 Mai 2019'),
	(15, 3, 3, NULL, 2, 3, 4, '9 Juillet 2019'),
	(16, 10, 1, NULL, 2, 5, 5, '7 Juin 2019');
/*!40000 ALTER TABLE `commander` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. etatcommande
DROP TABLE IF EXISTS `etatcommande`;
CREATE TABLE IF NOT EXISTS `etatcommande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.etatcommande : ~5 rows (environ)
DELETE FROM `etatcommande`;
/*!40000 ALTER TABLE `etatcommande` DISABLE KEYS */;
INSERT INTO `etatcommande` (`id`, `libelle`) VALUES
	(1, 'expedie'),
	(2, 'valide'),
	(3, 'en cours'),
	(4, 'refuse'),
	(5, 'panier');
/*!40000 ALTER TABLE `etatcommande` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. etatfacture
DROP TABLE IF EXISTS `etatfacture`;
CREATE TABLE IF NOT EXISTS `etatfacture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.etatfacture : ~3 rows (environ)
DELETE FROM `etatfacture`;
/*!40000 ALTER TABLE `etatfacture` DISABLE KEYS */;
INSERT INTO `etatfacture` (`id`, `libelle`) VALUES
	(1, 'realise'),
	(2, 'en cours'),
	(3, 'non realise');
/*!40000 ALTER TABLE `etatfacture` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. facture
DROP TABLE IF EXISTS `facture`;
CREATE TABLE IF NOT EXISTS `facture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moyenpaiementid` int(11) DEFAULT '0',
  `etatfactureid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_facture_etatfacture` (`etatfactureid`),
  KEY `FK_facture_moyenpaiement` (`moyenpaiementid`),
  CONSTRAINT `FK_facture_etatfacture` FOREIGN KEY (`etatfactureid`) REFERENCES `etatfacture` (`id`),
  CONSTRAINT `FK_facture_moyenpaiement` FOREIGN KEY (`moyenpaiementid`) REFERENCES `moyenpaiement` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.facture : ~3 rows (environ)
DELETE FROM `facture`;
/*!40000 ALTER TABLE `facture` DISABLE KEYS */;
INSERT INTO `facture` (`id`, `moyenpaiementid`, `etatfactureid`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 1, 3);
/*!40000 ALTER TABLE `facture` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. indiceenergetique
DROP TABLE IF EXISTS `indiceenergetique`;
CREATE TABLE IF NOT EXISTS `indiceenergetique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.indiceenergetique : ~3 rows (environ)
DELETE FROM `indiceenergetique`;
/*!40000 ALTER TABLE `indiceenergetique` DISABLE KEYS */;
INSERT INTO `indiceenergetique` (`id`, `libelle`) VALUES
	(1, 'Classe A'),
	(2, 'Classe B'),
	(3, 'Classe C');
/*!40000 ALTER TABLE `indiceenergetique` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. moyenpaiement
DROP TABLE IF EXISTS `moyenpaiement`;
CREATE TABLE IF NOT EXISTS `moyenpaiement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.moyenpaiement : ~2 rows (environ)
DELETE FROM `moyenpaiement`;
/*!40000 ALTER TABLE `moyenpaiement` DISABLE KEYS */;
INSERT INTO `moyenpaiement` (`id`, `libelle`) VALUES
	(1, 'carte bancaire'),
	(2, 'paypal');
/*!40000 ALTER TABLE `moyenpaiement` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. produit
DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `quantitestock` int(11) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `prixht` float DEFAULT NULL,
  `niveauenergetiqueid` int(11) DEFAULT NULL,
  `tvaid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_produit_tva` (`tvaid`),
  KEY `FK_produit_indiceenergetique` (`niveauenergetiqueid`),
  CONSTRAINT `FK_produit_indiceenergetique` FOREIGN KEY (`niveauenergetiqueid`) REFERENCES `indiceenergetique` (`id`),
  CONSTRAINT `FK_produit_tva` FOREIGN KEY (`tvaid`) REFERENCES `tva` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.produit : ~6 rows (environ)
DELETE FROM `produit`;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` (`id`, `libelle`, `description`, `quantitestock`, `image`, `prixht`, `niveauenergetiqueid`, `tvaid`) VALUES
	(1, 'machine à laver', 'Ceci est la description de la machine à laver', 25, NULL, 160, 1, 1),
	(2, 'ampoules', 'Ceci est la description de l\'ampoule', 60, NULL, 4.5, 2, 1),
	(3, 'four', 'Ceci est la description du four', 50, NULL, 200, 3, 1),
	(4, 'micro ondes', 'Ceci est la description du micro onde', 40, NULL, 123.45, 2, 1),
	(5, 'télévision', 'Ceci est la description de la télévision', 200, NULL, 680, 2, 1),
	(6, 'voiture', 'Ceci est la description de la voiture', 1, NULL, 25600, 1, 1);
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.role : ~3 rows (environ)
DELETE FROM `role`;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `libelle`) VALUES
	(1, 'administrateur'),
	(2, 'particulier'),
	(3, 'professionnel');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. service
DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `prixjourht` float DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  `tvaid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_service_tva` (`tvaid`),
  CONSTRAINT `FK_service_tva` FOREIGN KEY (`tvaid`) REFERENCES `tva` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.service : ~3 rows (environ)
DELETE FROM `service`;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` (`id`, `libelle`, `description`, `prixjourht`, `image`, `duree`, `tvaid`) VALUES
	(1, 'Collecte des cartons', 'Ceci est la description de la collecte de carton', 2, NULL, 3, 1),
	(2, 'Collecte du verre', 'Ceci est la description de la collecte du verre', 1.5, NULL, 6, 1),
	(3, 'Collecte des ordures', 'Ceci est la description de la collecte des ordures', 1, NULL, 12, 1);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;

-- Listage de la structure de la table eco_service. tva
DROP TABLE IF EXISTS `tva`;
CREATE TABLE IF NOT EXISTS `tva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taux` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Listage des données de la table eco_service.tva : ~1 rows (environ)
DELETE FROM `tva`;
/*!40000 ALTER TABLE `tva` DISABLE KEYS */;
INSERT INTO `tva` (`id`, `taux`) VALUES
	(1, 1.2);
/*!40000 ALTER TABLE `tva` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
