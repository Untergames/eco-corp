<div class="row">
    <div class="col-12">
        <h3 class="panel-title">Connexion</h3>
    </div>
</div>


<form role="form" method="post" action="index.php?uc=authentification&action=valideConnexion">
    <div class="row">
        <div class="col-md-12"> 
            <div class="form-group">
                <label for="">Adresse e-mail</label>
                <input class="form-control" placeholder="john@doe.com" name="email" type="email">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="form-group">
                <label>Mot de passe</label>
                <input class="form-control" placeholder="Mot de passe" name="password" type="password" maxlength="45">
            </div>
        </div>
    </div>


    <button class="btn btn-lg btn-success btn-block" type="submit">Se connecter</button>
    <a href="index.php?uc=authentification&action=demandeInscription" class="btn btn-primary btn-block">Pas encore inscrit ? Inscrivez-vous !</a>
</form>