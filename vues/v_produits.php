<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">libelle</th>
            <th scope="col">description</th>
            <th scope="col">quantite</th>
            <th scope="col">prixHT</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($produits as $unproduit) {
            $id = $unproduit['id'];
            $libelle = $unproduit['libelle'];
            $description = $unproduit['description'];
            $quantite = $unproduit['quantitestock'];
            $prixHT = $unproduit['prixht'];
            ?>
            <tr>
                <th scope="row"><?=$id ?></th>
                <td><?php echo $libelle ?></td>
                <td><?php echo $description ?></td>
                <td><?php echo $quantite ?></td>
                <td><?php echo $prixHT ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>