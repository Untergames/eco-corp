<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Eco-Service</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if (!$uc || $uc == 'accueil') { ?>active<?php } ?>">
                <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?uc=produits&action=getAll">Produits</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Espace membre
                </a>
                <?php if (estConnecte()) { ?>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Mon compte</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="index.php?uc=authentification&action=demandeDeconnexion"><i class="fas fa-power-off"></i> Déconnexion</a>
                    </div>
                <?php } else { ?>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="index.php?uc=authentification&action=demandeConnexion"><i class="fas fa-sign-in-alt"></i> Connexion</a>
                        <a class="dropdown-item" href="index.php?uc=authentification&action=demandeInscription"><i class="fas fa-user-plus"></i> Inscription</a>
                    </div>
                <?php } ?>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
        </form>
    </div>
</nav>