<div class="row">
    <div class="col-12">
        <h3 class="panel-title">Inscription</h3>
    </div>
</div>


<form role="form" method="post" action="index.php?uc=authentification&action=valideInscription">
    <div class="row">
        <div class="col-md-12"> 
            <div class="form-group">
                <label for="">Adresse e-mail</label>
                <input class="form-control" placeholder="john@doe.com" name="email" type="email" maxlength="45">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"> 
            <div class="form-group">
                <label>Mot de passe</label>
                <input class="form-control" placeholder="Mot de passe" name="password" type="password" maxlength="45">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Confirmation mot de passe</label>
                <input class="form-control" placeholder="Confirmation mot de passe" name="repeat-password" type="password" maxlength="45">
            </div>
        </div>
    </div>


    <button class="btn btn-lg btn-success btn-block" type="submit">Créer son compte</button>
    <a href="index.php?uc=authentification&action=demandeConnexion" class="btn btn-primary btn-block">Déjà un compte ? Connectez-vous</a>
</form>